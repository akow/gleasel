#!/bin/sh

prefix=$1
jar=$2


unzip "$1$2.jar" -d $2

search_dir="$1/lib"
for lib in "$search_dir"/*
do
	unzip $lib -x "META-INF/*" -d $2
done

cd $2

sed --in-place '/lib/d' META-INF/MANIFEST.MF

zip -r $2 *

cd ..

mv "$2/$2.zip" "$2.jar"

rm -rf $2
