/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author andrew
 */
public class ThreepartJFrame extends JFrame {

  public static final Dimension default_dimensions = new Dimension(1080, 720);
  private JPanel main_panel = new JPanel();
  private Component left_panel;
  private Component mid_panel;
  private Component right_panel;

  public ThreepartJFrame(Component left_panel, Component mid_panel, Component right_panel) {
    this.left_panel = left_panel;
    this.mid_panel = mid_panel;
    this.right_panel = right_panel;

    main_panel.setBackground(Color.WHITE);
    main_panel.setLayout(new GridBagLayout());

    /*
     ** Setup left panel
     */
    main_panel.add(left_panel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
     GridBagConstraints.WEST, GridBagConstraints.NONE,
     new Insets(10, 10, 10, 10), 0, 0));

    /*
     ** Setup right panel
     */
    main_panel.add(right_panel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
     GridBagConstraints.EAST, GridBagConstraints.NONE,
     new Insets(10, 10, 10, 10), 0, 0));

    /*
     ** Setup middle panel
     */
    main_panel.add(mid_panel, new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0,
     GridBagConstraints.WEST, GridBagConstraints.BOTH,
     new Insets(0, 0, 0, 0), 0, 0));

    getContentPane().add(main_panel);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(default_dimensions);
    setLocationRelativeTo(null);
  }

  public static void main(String[] args) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        JPanel jPanel1 = new JPanel();
        jPanel1.setMinimumSize(new Dimension(300, 500));
        jPanel1.setBackground(Color.red);
        jPanel1.setPreferredSize(new Dimension(300, 500));

        JButton jButton = new JButton("hi");
        jButton.setMinimumSize(new Dimension(0, 0));
        jButton.setBackground(Color.yellow);
        jButton.setPreferredSize(new Dimension(300, 500));

        JPanel jPanel2 = new JPanel();
        jPanel2.setMinimumSize(new Dimension(300, 500));
        jPanel2.setBackground(Color.blue);
        jPanel2.setPreferredSize(new Dimension(300, 500));

        new ThreepartJFrame(jPanel1, jButton, jPanel2).setVisible(true);
      }
    });
  }

  /**
   * @return the left_panel
   */
  public Component getLeft_panel() {
    return left_panel;
  }

  /**
   * @return the mid_panel
   */
  public Component getMid_panel() {
    return mid_panel;
  }

  /**
   * @return the right_panel
   */
  public Component getRight_panel() {
    return right_panel;
  }
}
