/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gleasel.components.functionalListeningGLCanvas;

import gleasel.io.Mousemanager;
import java.awt.Component;

/**
 *
 * @author andrew
 */
public interface ListeningInput<M extends Mousemanager> {
  Listeningcomponent<M> getListeningcomponent();
}
