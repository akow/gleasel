/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.functionalListeningGLCanvas;

import gleasel.io.Keymanager;
import gleasel.io.Mousemanager;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrew
 */
public class Listeningcomponent<Mm extends Mousemanager> implements MouseListener, MouseMotionListener, KeyListener {

  /**
   * @return the component
   */
  public Component getComponent() {
    return component;
  }

  public interface MousemanagerFactory<Mm> {

    Mm create();
  }

  private final Keymanager keymanager;
  private final Mm mousemanager;
  private boolean is_listening;
  private final Component component;
  private boolean running;

  public Listeningcomponent(MousemanagerFactory<Mm> mousemanagerFactory, Component component) {
    this(mousemanagerFactory.create(), component);
  }

  public Listeningcomponent(Mm mousemanager, Component component) {
    this.keymanager = new Keymanager();
    this.mousemanager = mousemanager;
    this.component = component;
    this.is_listening = false;
//    this.component.setFocusable(true);
//    this.component.addKeyListener(this);
//    this.component.addMouseMotionListener(this);
//    this.component.addMouseListener(this);
//    this.component.setVisible(true);
    stopListening();

  }


  public Keymanager getKeymanager() {
    return keymanager;
  }

  public Mm getMousemanager() {
    return mousemanager;
  }

  public void startListening() {
    if (this.is_listening) {
      stopListening();
    }
    //   this.component.enableInputMethods(true);
    this.component.setFocusable(true);
    this.component.requestFocus();
    this.component.addKeyListener(this);
    this.component.addMouseMotionListener(this);
    this.component.addMouseListener(this);
    this.keymanager.reset();
    this.mousemanager.reset();
    this.is_listening = true;

  }

  public void stopListening() {
    if (!this.is_listening) {
      return;
    }
    //this.component.enableInputMethods(false);
    this.keymanager.reset();
    this.mousemanager.reset();
    this.component.setFocusable(false);
    this.component.removeKeyListener(this);
    this.component.removeMouseMotionListener(this);
    this.component.removeMouseListener(this);
    this.is_listening = false;
  }

  public boolean isListening() {
    return this.is_listening;
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    mousemanager.mouseClicked(e);
  }

  @Override
  public void mousePressed(MouseEvent e) {
    mousemanager.mousePressed(e);
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    mousemanager.mouseReleased(e);
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    mousemanager.mouseEntered(e);
  }

  @Override
  public void mouseExited(MouseEvent e) {
    mousemanager.mouseExited(e);
  }

  @Override
  public void mouseDragged(MouseEvent e) {
    mousemanager.mouseDragged(e);
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    mousemanager.mouseMoved(e);
  }

  @Override
  public void keyTyped(KeyEvent e) {
    this.keymanager.keyTyped(e);
  }

  @Override
  public void keyPressed(KeyEvent e) {
    this.keymanager.keyPressed(e);
  }

  @Override
  public void keyReleased(KeyEvent e) {
    this.keymanager.keyReleased(e);
  }

  public void processActive() {
    this.keymanager.keyActive();
    this.mousemanager.mouseActive();
  }
}
