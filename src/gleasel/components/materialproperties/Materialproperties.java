/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.materialproperties;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JPanel;
import sun.awt.HorizBagLayout;

/**
 *
 * @author andrew
 */
public class Materialproperties extends javax.swing.JPanel {

  /**
   * @return the properties
   */
  public Properties getProperties() {
    return properties;
  }

  public interface Uniformscale {

    public float getValue();

  }

  public class RGBA {

    private Uniformscale red;
    private Uniformscale green;
    private Uniformscale blue;
    private Uniformscale alpha;

    public RGBA(Uniformscale red, Uniformscale green, Uniformscale blue, Uniformscale alpha) {
      this.red = red;
      this.green = green;
      this.blue = blue;
      this.alpha = alpha;
    }

    public float[] getRGBA() {
      return new float[]{
        this.red.getValue(),
        this.green.getValue(),
        this.blue.getValue(),
        this.alpha.getValue()
      };
    }

    /**
     * @return the red
     */
    public Uniformscale getRed() {
      return red;
    }

    /**
     * @return the green
     */
    public Uniformscale getGreen() {
      return green;
    }

    /**
     * @return the blue
     */
    public Uniformscale getBlue() {
      return blue;
    }

    /**
     * @return the alpha
     */
    public Uniformscale getAlpha() {
      return alpha;
    }

  }

  public class Properties {

    private final RGBA ambient;
    private final RGBA diffuse;
    private final RGBA specular;
    private final RGBA emission;
    private final Uniformscale shininess;

    public Properties(RGBA ambient, RGBA diffuse, RGBA specular, RGBA emission, Uniformscale shininess) {
      this.ambient = ambient;
      this.diffuse = diffuse;
      this.specular = specular;
      this.emission = emission;
      this.shininess = shininess;
    }

    /**
     * @return the ambient
     */
    public RGBA getAmbient() {
      return ambient;
    }

    /**
     * @return the diffuse
     */
    public RGBA getDiffuse() {
      return diffuse;
    }

    /**
     * @return the specular
     */
    public RGBA getSpecular() {
      return specular;
    }

    /**
     * @return the emission
     */
    public RGBA getEmission() {
      return emission;
    }

    /**
     * @return the shininess
     */
    public Uniformscale getShininess() {
      return shininess;
    }

  }

  public static Dimension getTabDimension(Tab tab) {
    Dimension d_loc = new Dimension();
    Iterator it2 = tab.sliders.entrySet().iterator();
    while (it2.hasNext()) {
      Map.Entry pairs2 = (Map.Entry) it2.next();
      Slider s = (Slider) pairs2.getValue();

      d_loc.width = Math.max(d_loc.width, s.getPreferredSize().width);
      d_loc.height += s.getPreferredSize().height;

      //it2.remove(); // avoids a ConcurrentModificationException
    }

    return d_loc;
  }

  public static class Tab {

    private HashMap<String, Slider> sliders;

    public Tab(String title, ArrayList<String> sliders) {
      this.sliders = new HashMap<String, Slider>();
      for (String slider_label : sliders) {
        this.sliders.put(slider_label, new Slider(slider_label));
      }
    }

    public HashMap<String, Slider> getSliders() {
      return sliders;
    }
  }

  private final Properties properties;

  /**
   * Creates new form Materialproperties
   */
  public Materialproperties() {
    initComponents();
    JPanel panel = new JPanel();
    panel.setLayout(new HorizBagLayout());
    final Slider shine = new Slider("Shine");
    panel.add(shine);
    jTabbedPane1.addTab("Shininess", panel);
    properties = new Properties(
     addRGBATab("ambient"),
     addRGBATab("diffuse"),
     addRGBATab("specular"),
     addRGBATab("emission"),
     new Uniformscale() {

       @Override
       public float getValue() {
         return shine.getValue();
       }
     });
    this.setPreferredSize(jTabbedPane1.getPreferredSize());
  }

  private RGBA addRGBATab(String name) {
    /*
     ** Create tabs
     */
    // Ambient

    JPanel panel = new JPanel();
    panel.setLayout(new HorizBagLayout());
    final Slider red_slider = new Slider("Red");
    final Slider green_slider = new Slider("Green");
    final Slider blue_slider = new Slider("Blue");
    final Slider alpha_slider = new Slider("Alpha");
    panel.add(red_slider);
    panel.add(green_slider);
    panel.add(blue_slider);
    panel.add(alpha_slider);
    RGBA rgba = new RGBA(
     new Uniformscale() {

       @Override
       public float getValue() {
         return red_slider.getValue();
       }
     }, new Uniformscale() {

       @Override
       public float getValue() {
         return green_slider.getValue();
       }
     }, new Uniformscale() {

       @Override
       public float getValue() {
         return blue_slider.getValue();
       }
     }, new Uniformscale() {

       @Override
       public float getValue() {
         return alpha_slider.getValue();
       }
     });
    jTabbedPane1.addTab(name, panel);
    return rgba;
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jTabbedPane1 = new javax.swing.JTabbedPane();

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
    );
  }// </editor-fold>//GEN-END:initComponents


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JTabbedPane jTabbedPane1;
  // End of variables declaration//GEN-END:variables

}
