/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.support;

import gleasel.components.Preferedsize;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.PrintStream;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import other.TextAreaOutputStream;

/**
 *
 * @author andrew
 */
public class Logger extends JPanel implements Preferedsize{

  public Logger() {
    super(false);

    JTextArea ta = new JTextArea();
    TextAreaOutputStream taos = new TextAreaOutputStream(ta, text_width);
    PrintStream ps = new PrintStream(taos);

    System.setOut(ps);
    System.setErr(ps);

    add(new JScrollPane(ta));
    setLayout(new GridLayout(1, 1));
  }
  private static final int text_width = 60;

  @Override
  public Dimension getPreferedSize() {
    return new Dimension(text_width, 50);
  }
}
