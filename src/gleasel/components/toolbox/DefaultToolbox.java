/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.toolbox;

import gleasel.components.toolbox.tools.BrushTool;
import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author andrew
 */
public class DefaultToolbox extends Toolbox {

  public static HashMap<String, JComponent> getDefaultTools() {
    HashMap<String, JComponent> tools = new HashMap<String, JComponent>();
    tools.put("Brush", new BrushTool());
    return tools;
  }

  public DefaultToolbox(HashMap<String, JComponent> tools) {
    super(new ToolmenuFactory() {

      @Override
      public Toolmenu create(HashMap<String, JComponent> tools, Toolmenu.Toolchanged toolchanged) {
        return new DefaultToolmenu(tools, toolchanged);
      }
    }, tools);
  }
}
