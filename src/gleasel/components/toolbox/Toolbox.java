/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.toolbox;

import gleasel.components.Preferedsize;
import gleasel.components.ThreepartJFrame;
import gleasel.components.toolbox.tools.NoTool;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.logging.Level;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import other.Jbp_Logger;

/**
 *
 * @author andrew
 */
public class Toolbox extends JScrollPane {

  private final Toolmenu tool_menu;
  private final JPanel tool_panel = new JPanel();
  private JPanel panel = new JPanel();
  private static final Dimension prefered_size = new Dimension(300, ThreepartJFrame.default_dimensions.height);
  private JComponent tool;
  protected HashMap<String, JComponent> tools;

  public Toolbox(ToolmenuFactory mytool_menu, HashMap<String, JComponent> tools) {
    this.tool_menu = mytool_menu.create(tools, new Toolmenu.Toolchanged() {

      @Override
      public void triggerToolChange() {
        toolHasBeenChanged(tool_menu.getSelected());
      }
    });
    this.tools = tools;
    this.setPreferredSize(prefered_size);
    this.setMinimumSize(prefered_size);

    this.panel = new JPanel();
    this.panel.setLayout(new GridLayout(2, 1));

    this.panel.add(this.tool_menu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
     GridBagConstraints.NORTH, GridBagConstraints.BOTH,
     new Insets(5, 5, 5, 5), 0, 0));

    this.panel.add(this.tool_panel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
     GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
     new Insets(5, 5, 5, 5), 0, 0));

    this.add(panel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
     GridBagConstraints.CENTER, GridBagConstraints.NONE,
     new Insets(5, 5, 5, 5), 5, 5));
    this.setViewportView(this.panel);

    this.tool_menu.toolChanged.triggerToolChange();
  }

  private JComponent getTool(String selected) {
    return tools.get(selected);
  }

  protected void toolHasBeenChanged(String new_tool_name) {
    tool = getTool(new_tool_name);
    if (tool == null) {
      tool = new NoTool();
    }

    tool_panel.removeAll();
    javax.swing.GroupLayout tool_panelLayout = new javax.swing.GroupLayout(tool_panel);
    tool_panel.setLayout(tool_panelLayout);
    tool_panelLayout.setHorizontalGroup(
     tool_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGroup(tool_panelLayout.createSequentialGroup()
      .addGap(0, 0, 0)
      .addComponent(tool)
      .addContainerGap(0, Short.MAX_VALUE))
    );
    tool_panelLayout.setVerticalGroup(
     tool_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
     .addGroup(tool_panelLayout.createSequentialGroup()
      .addGap(0, 0, 0)
      .addComponent(tool)
      .addContainerGap(0, Short.MAX_VALUE))
    );

    // New tool selected, repaint
    tool_panel.validate();
    tool_panel.repaint();
    panel.validate();
    panel.repaint();
    validate();
    repaint();
    Jbp_Logger.getLogger().log(Level.INFO, "Using tool: " + tool);
  }

}
