/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.components.toolbox;

import java.util.HashMap;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author andrew
 */
public abstract class Toolmenu extends JPanel {

  public interface Toolchanged {

    void triggerToolChange();
  }

  protected HashMap<String, JComponent> tools;
  protected Toolchanged toolChanged;

  public Toolmenu(HashMap<String, JComponent> tools, Toolchanged toolchanged) {
    this.tools = tools;
    this.toolChanged = toolchanged;
  }

  public abstract String getSelected();

}
