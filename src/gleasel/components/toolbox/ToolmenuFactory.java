/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gleasel.components.toolbox;

import gleasel.components.toolbox.Toolmenu.Toolchanged;
import java.util.HashMap;
import javax.swing.JComponent;

/**
 *
 * @author andrew
 */
public interface ToolmenuFactory {
  Toolmenu create(HashMap<String, JComponent> tools,Toolchanged toolchanged);
}
