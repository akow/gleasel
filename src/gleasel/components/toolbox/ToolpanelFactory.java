/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gleasel.components.toolbox;

/**
 *
 * @author andrew
 */
public interface ToolpanelFactory {
  Toolpanel create();
}
