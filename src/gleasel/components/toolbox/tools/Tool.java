/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gleasel.components.toolbox.tools;

import javax.swing.JComponent;

/**
 *
 * @author andrew
 */
public abstract class Tool extends JComponent{
  public abstract String toString();
}
