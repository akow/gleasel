package gleasel.easels;

import gleasel.components.functionalListeningGLCanvas.EGLCanvas;
import gleasel.components.functionalListeningGLCanvas.Listeningcomponent;
import gleasel.io.FocusedMousemanager;
import gleasel.io.Keymanager;
import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

/**
 *
 * @author andrew
 */
public class Easel extends JFrame {

  private final EGLCanvas listening_glcanvas;
  private final Listeningcomponent<FocusedMousemanager> listening_frame;

  public Easel(final EGLCanvas listening_glcanvas) throws AWTException {

    /*
     ** Setup the listening frame
     */
    FocusedMousemanager mousemanager = new FocusedMousemanager();
    this.listening_frame = new Listeningcomponent<FocusedMousemanager>(mousemanager, this);
    this.listening_frame.getComponent().setVisible(true);

    /*
     ** Setup the listening glcanvas
     */
    this.listening_glcanvas = listening_glcanvas;
    this.listening_glcanvas.getListeningcomponent().getComponent().setVisible(true);

    /*
     ** Setup ESC between canvas and easel
     */
    this.listening_frame.getKeymanager().registerKeyPressed(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), new Keymanager.SimpleAction() {

      @Override
      public void action() {
        if (!listening_glcanvas.getListeningcomponent().isListening()) {
          listening_frame.stopListening();
          listening_glcanvas.getListeningcomponent().startListening();

          /*
           ** Set a blank mouse cursor
           */
          BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
          Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(), "blank cursor");
          listening_glcanvas.getListeningcomponent().getComponent().setCursor(blankCursor);

          System.out.println(getDescription());
        }
      }

      @Override
      public String getDescription() {
        return "Toggles canvas mode";
      }
    });
    this.listening_glcanvas.getListeningcomponent().getKeymanager().registerKeyPressed(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), new Keymanager.SimpleAction() {

      @Override
      public void action() {
        if (listening_glcanvas.getListeningcomponent().isListening()) {
          listening_glcanvas.getListeningcomponent().stopListening();
          listening_frame.startListening();

          /*
           ** Set a mouse cursor
           */
          listening_glcanvas.getListeningcomponent().getComponent().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

          System.out.println(getDescription());
        }
      }

      @Override
      public String getDescription() {
        return "Escapes canvas mode";
      }
    });

    this.listening_frame.startListening();

    new Thread(new Runnable() {

      @Override
      public void run() {
        while (isVisible()) {
          try {
            if (listening_frame.isListening()) {
              requestFocusInWindow();
            }
            Thread.sleep(100);
          } catch (InterruptedException ex) {
            Logger.getLogger(Easel.class.getName()).log(Level.SEVERE, null, ex);
          }
        }
      }
    }).start();
  }

}
