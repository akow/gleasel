/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.easels.internalframe;

import com.jogamp.opengl.util.Animator;
import gleasel.easels.Easel;
import gleasel.fpc.FunctionalFpc;
import gleasel.components.functionalListeningGLCanvas.EGLCanvas;
import gleasel.components.materialproperties.Materialproperties;
import gleasel.components.materialproperties.Slider;
import gleasel.components.support.Palette;
import gleasel.components.toolbox.DefaultToolbox;
import gleasel.components.toolbox.Toolbox;
import gleasel.components.toolbox.tools.Shapes;
import gleasel.io.CenteredMousemanager;
import gleasel.io.Mousemanager;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.vecmath.Color3f;
import javax.vecmath.Color4f;
import javax.vecmath.Point3f;
import other.Jbp_Logger;

/**
 *
 * @author andrew
 */
public class InternalframeEasel extends Easel implements ActionListener {

  public interface Action {

    void action();
  }

  private JDesktopPane desktop;
  private static final float init_window_size_factor = 0.65f;
  private JMenuBar menu_bar = new JMenuBar();
  private JMenu menu_windows = new JMenu("Windows");

  private HashMap<String, Action> actions_list = new HashMap<String, Action>();

  public InternalframeEasel(EGLCanvas<CenteredMousemanager> eGLCanvas) throws AWTException {
    super(eGLCanvas);

    Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0].getDefaultConfiguration().getBounds();

    Dimension screenSize = new Dimension(bounds.width, bounds.height);
    setBounds((int) (screenSize.width * (.5f - init_window_size_factor / 2f)),
     (int) (screenSize.height * (.5f - init_window_size_factor / 2f)),
     (int) (screenSize.width * init_window_size_factor),
     (int) (screenSize.height * init_window_size_factor));

    //Set up the GUI.
    desktop = new JDesktopPane(); //a specialized layered pane

    eGLCanvas.setSize(this.getSize());
    desktop.add(eGLCanvas);

    setContentPane(desktop);
    createBasicMenuOptions();
    setJMenuBar(menu_bar);

    //Make dragging a little faster but perhaps uglier.
    desktop.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);

  }

  public void addWindow(final JInternalFrame window, final Point default_location) {
    try {
      window.setVisible(true); //necessary as of 1.3
      desktop.add(window);
      window.setSelected(true);
      window.setLocation(default_location);
      window.setClosable(true);

      //Set up the second menu item.
      JMenuItem window_tab = new JMenuItem(window.getTitle());
//      window_tab.setMnemonic(KeyEvent.VK_Q);
//      window_tab.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.ALT_MASK));
      window_tab.setActionCommand(window.getTitle());
      window_tab.addActionListener(this);
      actions_list.put(window.getTitle(), new Action() {

        final Point oDefault_location = new Point(default_location);
        final JInternalFrame owindow = window;

        @Override
        public void action() {
          if (!new ArrayList<JInternalFrame>(Arrays.asList(desktop.getAllFrames())).contains(owindow)) {
            try {
              window.setVisible(true); //necessary as of 1.3
              desktop.add(window);
              window.setSelected(true);
              window.setLocation(default_location);
              Jbp_Logger.getLogger().log(Level.INFO, "Loaded " + owindow.getTitle());
            } catch (PropertyVetoException ex) {
              Logger.getLogger(InternalframeEasel.class.getName()).log(Level.SEVERE, null, ex);
            }
          } else {
            owindow.setVisible(!owindow.isVisible());
            if (owindow.isVisible()) {
              Jbp_Logger.getLogger().log(Level.INFO, owindow.getTitle() + " is now visible");
            } else {
              Jbp_Logger.getLogger().log(Level.INFO, owindow.getTitle() + " is now hidden");
            }
          }
        }
      }
      );

      menu_windows.add(window_tab);

    } catch (PropertyVetoException ex) {
      Logger.getLogger(InternalframeEasel.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  protected void createBasicMenuOptions() {
    /*
     ** Setup menu file tab
     */
    JMenu menu_file = new JMenu("File");
    JMenuItem menu_new = new JMenuItem("Quit");
    menu_new.setMnemonic(KeyEvent.VK_Q);
    menu_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.ALT_MASK));
    menu_new.setActionCommand("Quit");
    actions_list.put("Quit", new Action() {

      @Override
      public void action() {
        quit();
      }
    });
    menu_new.addActionListener(this);
    menu_file.add(menu_new);
    menu_file.setMnemonic(KeyEvent.VK_F);
    menu_bar.add(menu_file);

    /*
     ** Setup menu windows tab
     */
    menu_windows.setMnemonic(KeyEvent.VK_W);
    menu_bar.add(menu_windows);

  }

  //React to menu selections.
  public void actionPerformed(ActionEvent e) {
//    Iterator it = actions_list.entrySet().iterator();
//    while (it.hasNext()) {
//      Map.Entry pairs = (Map.Entry) it.next();
//      if (((String) pairs.getKey()).equalsIgnoreCase(e.getActionCommand())) {
//        ((Action) pairs.getValue()).action();
//      }
//    }

  }

  //Quit the application.
  protected void quit() {
    System.exit(0);
  }

  public Point getLocation(JInternalFrame frame, float i, float j) {
    if (i < 0f || i > 1f) {
      Logger.getLogger(InternalframeEasel.class.getName()).log(Level.SEVERE, "Invalid value", new Exception("" + i));
    }
    if (j < 0f || j > 1f) {
      Logger.getLogger(InternalframeEasel.class.getName()).log(Level.SEVERE, "Invalid value", new Exception("" + j));
    }

    return new Point(
     (int) (i * (this.getSize().width - frame.getSize().width)),
     (int) (j * (this.getSize().height - frame.getSize().height))
    );
  }

  public static void main(String[] args) {
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        try {
          JFrame.setDefaultLookAndFeelDecorated(true);
          final FunctionalFpc functionalFpc = new FunctionalFpc();
          InternalframeEasel frame = new InternalframeEasel(functionalFpc);
          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          frame.setVisible(true);

          final Animator animator = new Animator(functionalFpc);
          frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
              new Thread(new Runnable() {
                public void run() {
                  animator.stop();
                  System.exit(0);
                }
              }).start();
            }
          });

          /*
           ** Add pallete
           */
          JInternalFrame jInternalFrame = new JInternalFrame();
          final Palette palette = new Palette();
          jInternalFrame.add(palette);
          jInternalFrame.setTitle("Palette");
          jInternalFrame.setSize(palette.getPreferredSize());
          jInternalFrame.setResizable(true);
          jInternalFrame.setClosable(true);
          frame.addWindow(jInternalFrame, frame.getLocation(jInternalFrame, .5f, .99f));

          /*
           ** Add Toolbox
           */
          final Shapes shapes = new Shapes(functionalFpc.getListOfAvailableShapes());
          HashMap<String, JComponent> tools = new HashMap<String, JComponent>();
          tools.put("Shapes", shapes);
          final Toolbox toolbox = new DefaultToolbox(tools);
          jInternalFrame = new JInternalFrame();
          jInternalFrame.add(toolbox);
          jInternalFrame.setTitle("Toolbox");
          jInternalFrame.setSize(toolbox.getPreferredSize());
          jInternalFrame.setResizable(true);
          jInternalFrame.setClosable(true);
          frame.addWindow(jInternalFrame, frame.getLocation(jInternalFrame, .01f, 0.5f));

          /*
           ** Add Materialproperties
           */
          HashMap<String, Materialproperties.Tab> tabs = new HashMap<String, Materialproperties.Tab>();

          final Materialproperties materialproperties = new Materialproperties();
          jInternalFrame = new JInternalFrame();
          jInternalFrame.add(materialproperties);
          jInternalFrame.setTitle("Material Properties");
          jInternalFrame.setSize(materialproperties.getPreferredSize());
          jInternalFrame.setResizable(true);
          jInternalFrame.setClosable(true);
          frame.addWindow(jInternalFrame, frame.getLocation(jInternalFrame, .99f, 0.5f));

          /*
           ** Add an feature
           */
          functionalFpc.getListeningcomponent().getMousemanager().registerMouseClicked(MouseEvent.BUTTON1, new Mousemanager.SimpleAction() {

            @Override
            public void action() {
              String selected = shapes.getSelected();
              if (selected != null) {
                Color3f color3f = new Color3f(palette.getjColorChooser1().getColor());

                functionalFpc.placeImageAt(new Color4f(color3f.x, color3f.y, color3f.z, palette.getAlpha()), selected,
                 FloatBuffer.wrap(materialproperties.getProperties().getAmbient().getRGBA()),
                 FloatBuffer.wrap(materialproperties.getProperties().getDiffuse().getRGBA()),
                 FloatBuffer.wrap(materialproperties.getProperties().getSpecular().getRGBA()),
                 FloatBuffer.wrap(materialproperties.getProperties().getEmission().getRGBA()),
                 FloatBuffer.wrap(new float[]{materialproperties.getProperties().getShininess().getValue()})
                );
              } else {
                System.out.println("No shape selected");
              }
            }
          });

          /*
           ** Add a starting shape
           */
          functionalFpc.placeImageAt(new Point3f(), new Color4f(0f, 0f, 1f, 1f), "glutSolidSphere",
           FloatBuffer.wrap(new float[]{.2f, .2f, .2f, 1f}),
           FloatBuffer.wrap(new float[]{0f, 0f, 0f, 0f}),
           FloatBuffer.wrap(new float[]{0f, 0f, 0f, 0f}),
           FloatBuffer.wrap(new float[]{0f, 0f, 0f, 0f}),
           FloatBuffer.wrap(new float[]{0f, 0f, 0f, 0f})
          );

          frame.setVisible(true);
          animator.start();
        } catch (AWTException ex) {
          Logger.getLogger(InternalframeEasel.class.getName()).log(Level.SEVERE, null, ex);
        }
      }

    });
  }

}
