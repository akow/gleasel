package gleasel.fpc;

import java.awt.Point;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.glu.GLU;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;


public class Fpc_camera
  implements GLBasefunctions {

  public Vector3d eye;
  private Vector3d up;
  public Vector2d rot;
  private Vector2d fov;

  public Fpc_camera(Vector3d eye) {
    this.eye = eye;
    this.rot = new Vector2d(0.0, 0.0);
    this.up = new Vector3d(0.0, 1.0, 0.0);
    this.fov = new Vector2d(0.0, 0.0);
  }

  public Fpc_camera(Vector3d eye, Vector2d rot) {
    this.eye = eye;
    this.rot = rot;
    this.up = new Vector3d(0.0, 1.0, 0.0);
    this.fov = new Vector2d(0.0, 0.0);
  }

  @Override
  public void init(GLAutoDrawable drawable) {

  }

  @Override
  public void display(GLAutoDrawable drawable) {
    // Change to projection matrix.
    drawable.getGL().getGL2().glMatrixMode(GLMatrixFunc.GL_PROJECTION);
    drawable.getGL().getGL2().glLoadIdentity();

    // Perspective.
    new GLU().gluPerspective(90, drawable.getWidth() / drawable.getHeight(), 1, 1000);
    drawable.getGL().getGL2().glRotated(rot.x, 0, 1, 0);
    drawable.getGL().getGL2().glRotated(rot.y, Math.cos(Math.toRadians(rot.x)), 0.0,
      Math.sin(Math.toRadians(rot.x)));
    new GLU().gluLookAt(eye.x, eye.y, eye.z,
      eye.x, eye.y, eye.z - 0.01,
      0, 1, 0);

    // Change back to model view matrix.
    drawable.getGL().getGL2().glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
    drawable.getGL().getGL2().glLoadIdentity();
  }

  public void move(double m_x, double m_y, double m_z) {
    double cz = m_z * Math.cos(Math.toRadians(rot.x));
    double cx = -m_z * Math.sin(Math.toRadians(rot.x));
    cz += m_x * Math.sin(Math.toRadians(rot.x));
    cx += m_x * Math.cos(Math.toRadians(rot.x));
    double cy = m_y;
    eye.x += cx;
    eye.y += cy;
    eye.z += cz;
  }

  void keyActive(boolean[] activekeys, double coeff) {
    if (activekeys['w']) {
      this.move(0.0, 0.0, -coeff);
    }
    if (activekeys['s']) {
      this.move(0.0, 0.0, coeff);
    }
    if (activekeys['a']) {
      this.move(-coeff, 0.0, 0.0);
    }
    if (activekeys['d']) {
      this.move(coeff, 0.0, 0.0);
    }
    if (activekeys['r']) {
      this.move(0.0, coeff, 0.0);
    }
    if (activekeys['f']) {
      this.move(0.0, -coeff, 0.0);
    }

  }

  void mouseChange(Point mDiff) {
    rot.x -= mDiff.x;
    rot.x %= 360.0;
    rot.y = Math.min(Math.max(rot.y - mDiff.y, -90.0), 90.0);
  }

  @Override
  public void render(GLAutoDrawable drawable) {

  }
}
