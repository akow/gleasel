package gleasel.fpc;

import java.awt.Point;
import java.awt.event.KeyEvent;
import javax.vecmath.*;

public class Fpc_entity {

    private final Fpc_camera camera ;

    public Fpc_entity(Vector3d eye, Vector2d rot) {
        camera = new Fpc_camera(eye, rot);
    }
    
    public void keyPressed(KeyEvent ke) {
               
    }

    public void keyReleased(KeyEvent ke) {
         
    }
    
    void keyActive(boolean[] activekeys, double coeff) {
        getCamera().keyActive(activekeys,coeff);
    }
    
    void mouseChange(Point mDiff) {
        getCamera().mouseChange(mDiff);
    }

  /**
   * @return the camera
   */
  public Fpc_camera getCamera() {
    return camera;
  }
    
}
