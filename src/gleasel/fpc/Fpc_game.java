package gleasel.fpc;

import java.awt.Point;
import java.awt.event.KeyEvent;
import javax.media.opengl.GLAutoDrawable;

/**
 * @tags: blocks project game head
 * @desc: Central control between menus, worlds, editors
 */
public class Fpc_game implements GLBasefunctions{

  private final Fpc_world world;

  public Fpc_game() {
    world = new Fpc_world();
  }

  public void init(GLAutoDrawable drawable){
    getWorld().init(drawable);
  }
  
  public void display(GLAutoDrawable drawable) {
    getWorld().display(drawable);
  }
 
  /**
   * @return the world
   */
  public Fpc_world getWorld() {
    return world;
  }

  @Override
  public void render(GLAutoDrawable drawable) {
    getWorld().render(drawable);
  }
}
