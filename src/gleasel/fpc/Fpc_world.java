package gleasel.fpc;

import com.jogamp.opengl.util.gl2.GLUT;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.vecmath.Color4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import javax.media.opengl.fixedfunc.GLLightingFunc;
/**
 * @tags blocks project world schematic adventure mode
 * @desc Parent and children classes of the game worlds
 */
public class Fpc_world implements GLBasefunctions {

  /**
   * @return the renderable_shapes
   */
  public HashMap<String, RenderShapeFunction> getRenderable_shapes() {
    return renderable_shapes;
  }

  private class Image {

    private FloatBuffer ambient;
    private FloatBuffer diffuse;
    private FloatBuffer specular;
    private FloatBuffer shine;
    private FloatBuffer emission;
    private int img;
    private Point3f coord;
    private Color4f color4f;
    private RenderShapeFunction function;

    public Image(FloatBuffer ambient, FloatBuffer diffuse, FloatBuffer specular, FloatBuffer shine, FloatBuffer emission, int img, Point3f coord, Color4f color4f, RenderShapeFunction function) {
      this.ambient = ambient;
      this.diffuse = diffuse;
      this.specular = specular;
      this.shine = shine;
      this.emission = emission;
      this.img = img;
      this.coord = coord;
      this.color4f = color4f;
      this.function = function;
    }

    public void display(GLAutoDrawable drawable) {
      drawable.getGL().getGL2().glPushMatrix();
      if (ambient != null) {
        drawable.getGL().getGL2().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_AMBIENT, ambient);
      }
      if (diffuse != null) {
        drawable.getGL().getGL2().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_DIFFUSE, diffuse);
      }
      if (specular != null) {
        drawable.getGL().getGL2().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_SPECULAR, specular);
      }
      if (shine != null) {
        drawable.getGL().getGL2().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_SHININESS, shine);
      }
      if (emission != null) {
        drawable.getGL().getGL2().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_EMISSION, emission);
      }
      drawable.getGL().getGL2().glCallList(img);
      drawable.getGL().getGL2().glPopMatrix();
    }

    /**
     * @return the ambient
     */
    public FloatBuffer getAmbient() {
      return ambient;
    }

    /**
     * @return the diffuse
     */
    public FloatBuffer getDiffuse() {
      return diffuse;
    }

    /**
     * @return the specular
     */
    public FloatBuffer getSpecular() {
      return specular;
    }

    /**
     * @return the shine
     */
    public FloatBuffer getShine() {
      return shine;
    }

    /**
     * @return the img
     */
    public int getImg() {
      return img;
    }

  }

  private Fpc_player player;
  private LinkedList<Image> toRender = new LinkedList<Image>();
  private LinkedList<Image> images = new LinkedList<Image>();
  private HashMap<String, RenderShapeFunction> renderable_shapes = new HashMap<String, RenderShapeFunction>();

  private Image pending = null;

  public Fpc_world() {
    this.player = new Fpc_player(new Vector3d(0.0, 0.0, 5.0), new Vector2d(0.0, 0.0));
    addSolidShapes();
    addWireShapes();
  }

  @Override
  public void init(GLAutoDrawable drawable) {
    getPlayer().getCamera().init(drawable);

  }

  public void addWireShapes() {
    getRenderable_shapes().put("glutWireCube", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutWireCube(1);
      }
    });
     getRenderable_shapes().put("glutWireSphere", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutWireSphere(1, 10, 10);
      }
    });
  }

  public void addSolidShapes() {
    getRenderable_shapes().put("glutSolidCone", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidCone(1, 1, 100, 100);
      }
    });
    getRenderable_shapes().put("glutSolidCube", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidCube(1f);
      }
    });
    getRenderable_shapes().put("glutSolidCylinder", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidCylinder(1, 1, 100, 100);
      }
    });
    getRenderable_shapes().put("glutSolidDodecahedron", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidDodecahedron();
      }
    });
    getRenderable_shapes().put("glutSolidIcosahedron", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidIcosahedron();
      }
    });
    getRenderable_shapes().put("glutSolidOctahedron", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidOctahedron();
      }
    });
    getRenderable_shapes().put("glutSolidRhombicDodecahedron", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidRhombicDodecahedron();
      }
    });
    getRenderable_shapes().put("glutSolidSphere", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidSphere(1, 100, 100);
      }
    });
    getRenderable_shapes().put("glutSolidTeapot", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidTeapot(1);
      }
    });
    getRenderable_shapes().put("glutSolidTetrahedron", new RenderShapeFunction() {

      @Override
      public void render(GLAutoDrawable drawable) {
        new GLUT().glutSolidTetrahedron();
      }
    });
  }

  @Override
  public void display(GLAutoDrawable drawable) {
    this.getPlayer().getCamera().display(drawable);
    for (Image images1 : images) {
      drawable.getGL().getGL2().glPushMatrix();
      images1.display(drawable);
      drawable.getGL().getGL2().glPopMatrix();
    }
  }

  /**
   * @return the player
   */
  public Fpc_player getPlayer() {
    return player;
  }

  @Override
  public void render(GLAutoDrawable drawable) {
    if (!toRender.isEmpty()) {
      Image image = toRender.poll();

      image.img = drawable.getGL().getGL2().glGenLists(1);

      drawable.getGL().getGL2().glNewList(image.img, GL2.GL_COMPILE);
      drawable.getGL().getGL2().glColor4f(image.color4f.x, image.color4f.y, image.color4f.z, image.color4f.w);
      drawable.getGL().getGL2().glTranslatef(image.coord.x, image.coord.y, image.coord.z);
      image.function.render(drawable);
      drawable.getGL().getGL2().glEndList();

      images.add(image);
    }
  }

  public void createImgAt(Point3f coord3f, Color4f color4f, RenderShapeFunction function,
    FloatBuffer ambient, FloatBuffer diffuse, FloatBuffer specular, FloatBuffer shine, FloatBuffer emission
  ) {
    toRender.add(new Image(ambient, diffuse, specular, shine, emission, 0, coord3f, color4f, function));
  }

}
