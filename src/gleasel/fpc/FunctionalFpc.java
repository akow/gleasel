/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.fpc;

import com.jogamp.opengl.util.Animator;
import gleasel.components.functionalListeningGLCanvas.Listeningcomponent;
import gleasel.components.functionalListeningGLCanvas.EGLCanvas;
import gleasel.io.CenteredMousemanager;
import gleasel.io.Keymanager;
import gleasel.io.Mousemanager;
import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import static javax.media.opengl.GLProfile.GL2;
import javax.media.opengl.fixedfunc.GLMatrixFunc;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.glu.GLU;
import javax.swing.KeyStroke;
import javax.vecmath.Color4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

/**
 *
 * @author andrew
 */
public class FunctionalFpc extends EGLCanvas<CenteredMousemanager> implements Imgplacer {

  private final Listeningcomponent<CenteredMousemanager> listeningcomponent;
  private final Fpc_game game;
  public static final int width = 800;
  public static final int height = 600;
  private double sensitivity = 10;

  FloatBuffer no_mat = FloatBuffer.wrap(new float[]{0.0f, 0.0f, 0.0f, 1.0f});
  FloatBuffer mat_ambient = FloatBuffer.wrap(new float[]{0.7f, 0.7f, 0.7f, 1.0f});
  FloatBuffer mat_ambient_color = FloatBuffer.wrap(new float[]{0.8f, 0.8f, 0.2f, 1.0f});
  FloatBuffer mat_diffuse = FloatBuffer.wrap(new float[]{0.1f, 0.5f, 0.8f, 1.0f});
  FloatBuffer mat_specular = FloatBuffer.wrap(new float[]{1.0f, 1.0f, 1.0f, 1.0f});
  FloatBuffer no_shininess = FloatBuffer.wrap(new float[]{0.0f});
  FloatBuffer low_shininess = FloatBuffer.wrap(new float[]{5.0f});
  FloatBuffer high_shininess = FloatBuffer.wrap(new float[]{100.0f});
  FloatBuffer mat_emission = FloatBuffer.wrap(new float[]{0.3f, 0.2f, 0.2f, 0.0f});

  public FunctionalFpc() throws AWTException {
    this.addGLEventListener(this);
    this.game = new Fpc_game();
    this.listeningcomponent = new Listeningcomponent<CenteredMousemanager>(new CenteredMousemanager(this), this);
    registerKeys();
    registerMouse();
  }

  public void init(GLAutoDrawable drawable) {
    // Use debug pipeline
    // drawable.setGL(new DebugGL(drawable.getGL()));

    // Enable VSync
    drawable.getGL().setSwapInterval(1);

    drawable.getGL().glEnable(GLLightingFunc.GL_COLOR_MATERIAL);
    // Setup the drawing area and shading mode
//    drawable.getGL().glEnable(GL.GL_DEPTH_TEST);
//    drawable.getGL().glDepthFunc(GL.GL_LEQUAL);
    drawable.getGL().glClearColor(173.0f / 255.0f, 216f / 255.0f, 230f / 255.0f, 0f);
    drawable.getGL().getGL2().glShadeModel(GLLightingFunc.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    //drawable.getGL().glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
    drawable.getGL().glDepthRange(0.001, 10000);

//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, FloatBuffer.wrap(new float[]{1.0f, 1.0f, 1.0f, 1f}));
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, FloatBuffer.wrap(new float[]{1.0f, 1.0f, 1.0f, 1f}));
    //drawable.getGL().glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_SHININESS, FloatBuffer.wrap(new float[]{50f}));
    //  drawable.getGL().glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, FloatBuffer.wrap(new float[]{1f, 1f, 1f, 1f}));
//    drawable.getGL().glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, FloatBuffer.wrap(new float[]{1f, 1f, 1f, 1f}));
//    drawable.getGL().glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, FloatBuffer.wrap(new float[]{1000f, 1000f, 1000f, 0f}));
    drawable.getGL().glEnable(GLLightingFunc.GL_LIGHTING);
    drawable.getGL().glEnable(GLLightingFunc.GL_LIGHT0);
    drawable.getGL().glEnable(GL.GL_DEPTH_TEST);
    drawable.getGL().glEnable(GL.GL_ALPHA);

    drawable.getGL().glEnable(GL.GL_BLEND);
    drawable.getGL().glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);

    this.getGame().init(drawable);
  }

  public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    GL gl = drawable.getGL();
    GLU glu = new GLU();

    if (height <= 0) { // avoid a divide by zero error!

      height = 1;
    }
    final float h = (float) width / (float) height;
    gl.glViewport(0, 0, width, height);
    gl.getGL2().glMatrixMode(GLMatrixFunc.GL_PROJECTION);
    gl.getGL2().glLoadIdentity();
    glu.gluPerspective(45.0f, h, 1.0, 20.0);
    gl.getGL2().glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
    gl.getGL2().glLoadIdentity();
  }

  public void display(GLAutoDrawable drawable) {
    game.render(drawable);
    listeningcomponent.processActive();

// Clear the drawing area
    drawable.getGL().glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
    // Reset the current matrix to the "identity"
    drawable.getGL().getGL2().glLoadIdentity();

    getGame().display(drawable);

//    /*  draw sphere in first row, first column
//     *  diffuse reflection only; no ambient or specular  
//     */
//    drawable.getGL().glPushMatrix();
//    drawable.getGL().glTranslated(-3.75, 3.0, 0.0);
//    drawable.getGL().glColor4d(1,0,0,1);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, no_shininess);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, no_mat);
//    new GLUT().glutSolidSphere(1, 100, 100);
//    drawable.getGL().glPopMatrix();
//
//    /*  draw sphere in first row, second column
//     *  diffuse and specular reflection; low shininess; no ambient
//     */
//    drawable.getGL().glPushMatrix();
//    drawable.getGL().glTranslated(-1.25, 3.0, 0.0);
//    drawable.getGL().glColor4d(1,0,1,1);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, low_shininess);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, no_mat);
//    new GLUT().glutSolidSphere(1, 100, 100);
//    drawable.getGL().glPopMatrix();
//
//    /*  draw sphere in first row, third column
//     *  diffuse and specular reflection; high shininess; no ambient
//     */
//    drawable.getGL().glPushMatrix();
//    drawable.getGL().glTranslated(1.25, 3.0, 0.0);
//    drawable.getGL().glColor4d(1,1,0,1);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, mat_specular);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, high_shininess);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, no_mat);
//    new GLUT().glutSolidSphere(1, 100, 100);
//    drawable.getGL().glPopMatrix();
//
//    /*  draw sphere in first row, fourth column
//     *  diffuse reflection; emission; no ambient or specular refl.
//     */
//    drawable.getGL().glPushMatrix();
//    drawable.getGL().glTranslated(3.75, 3.0, 0.0);
//    drawable.getGL().glColor4d(0,1,0,1);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, mat_diffuse);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, no_mat);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_SHININESS, no_shininess);
//    drawable.getGL().glMaterialfv(GL.GL_FRONT, GL.GL_EMISSION, mat_emission);
//    new GLUT().glutSolidSphere(1, 100, 100);
//    drawable.getGL().glPopMatrix();
    // Flush all drawing operations to the graphics card
    drawable.getGL().glFlush();

  }

  public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
  }

  /**
   * @return the game
   */
  public Fpc_game getGame() {
    return game;
  }

  private void registerKeys() {
    Keymanager keymanager = this.listeningcomponent.getKeymanager();
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_W, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(0.0, 0.0, -sensitivity * delta);
      }

      @Override
      public String getDescription() {
        return "Move forward";
      }
    });
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(0.0, 0.0, sensitivity * delta);
      }

      @Override
      public String getDescription() {
        return "Move backwards";
      }
    });
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_A, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(-sensitivity * delta, 0.0, 0.0);
      }

      @Override
      public String getDescription() {
        return "Move left";
      }
    });
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_D, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(sensitivity * delta, 0.0, 0.0);
      }

      @Override
      public String getDescription() {
        return "Move right";
      }
    });
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(0.0, sensitivity * delta, 0.0);
      }

      @Override
      public String getDescription() {
        return "Move Up";
      }
    });
    keymanager.registerKeyActive(KeyStroke.getKeyStroke(KeyEvent.VK_F, 0, false), new Keymanager.DeltaAction() {

      @Override
      public void action(double delta) {
        getGame().getWorld().getPlayer().getCamera().move(0.0, -sensitivity * delta, 0.0);
      }

      @Override
      public String getDescription() {
        return "Move Down";
      }
    });

  }

  private void registerMouse() {
    CenteredMousemanager centeredMousemanager = this.listeningcomponent.getMousemanager();
    centeredMousemanager.registerMouseMoved(new Mousemanager.ChangeAction() {

      @Override
      public void action(Point diff) {
        getGame().getWorld().getPlayer().getCamera().mouseChange(diff);
      }
    });
  }

  public static void main(String[] args) {
    System.out.println("FUCK");
//    try {
//      FunctionalFpc fpc = new FunctionalFpc();
//      final Animator animator = new Animator(fpc);
//
//      Frame frame = new Frame("Simple JOGL Application");
//      frame.addWindowListener(new WindowAdapter() {
//
//        @Override
//        public void windowClosing(WindowEvent e) {
//          new Thread(new Runnable() {
//            public void run() {
//              animator.stop();
//              System.exit(0);
//            }
//          }).start();
//        }
//      });
//
//      frame.add(fpc);
//      frame.setSize(width, height);
//      frame.setLocation(new Point(0, 0));
//      frame.setVisible(true);
//      animator.start();
//      fpc.getListeningcomponent().startListening();
//    } catch (AWTException ex) {
//      Logger.getLogger(FunctionalFpc.class.getName()).log(Level.SEVERE, null, ex);
//    }
  }

  @Override
  public Listeningcomponent<CenteredMousemanager> getListeningcomponent() {
    return listeningcomponent;
  }

  public void placeImageAt(Color4f color4f, String shape,
    FloatBuffer ambient, FloatBuffer diffuse, FloatBuffer specular, FloatBuffer shine, FloatBuffer emission) {
    Vector3d eye = getGame().getWorld().getPlayer().getCamera().eye;
    placeImageAt(new Point3f((float) eye.x, (float) eye.y, (float) eye.z), color4f, shape, ambient, diffuse, specular, shine, emission);
  }

  @Override
  public ArrayList<String> getListOfAvailableShapes() {
    HashMap<String, RenderShapeFunction> renderable_shapes = getGame().getWorld().getRenderable_shapes();

    ArrayList<String> shapes_list = new ArrayList<String>();

    Iterator it = renderable_shapes.entrySet().iterator();
    while (it.hasNext()) {
      Map.Entry pairs = (Map.Entry) it.next();
      shapes_list.add((String) pairs.getKey());
    }
    return shapes_list;
  }

  public void placeImageAt(Point3f coord, Color4f color, String shape,
    FloatBuffer ambient, FloatBuffer diffuse, FloatBuffer specular, FloatBuffer shine, FloatBuffer emission) {
    RenderShapeFunction get = getGame().getWorld().getRenderable_shapes().get(shape);
    if (get == null) {
      Logger.getLogger(FunctionalFpc.class.getName()).log(Level.SEVERE, "Shape does not exist", new Exception(shape));
    }
    getGame().getWorld().createImgAt(coord, color, get, ambient, diffuse, specular, shine, emission);
  }
 
}
