/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.fpc;

import javax.media.opengl.GLAutoDrawable;

/**
 *
 * @author andrew
 */
public interface GLBasefunctions {

  public void init(GLAutoDrawable drawable);

  public void render(GLAutoDrawable drawable);

  public void display(GLAutoDrawable drawable);
}
