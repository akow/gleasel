/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.fpc;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import javax.vecmath.Color4f;
import javax.vecmath.Point3f;

/**
 *
 * @author andrew
 */
public interface Imgplacer {

  public void placeImageAt(Point3f coord, Color4f color, String shape,
   FloatBuffer ambient, FloatBuffer diffuse, FloatBuffer specular, FloatBuffer shine, FloatBuffer emission);

  public ArrayList<String> getListOfAvailableShapes();
}
