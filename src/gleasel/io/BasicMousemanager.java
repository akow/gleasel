/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.io;

import java.awt.Frame;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

/**
 *
 * @author andrew
 */
public abstract class BasicMousemanager<
  MOUSE_CLICKED_ACTION extends Mousemanager.Action,//
 MOUSE_DRAGGED_ACTION extends Mousemanager.Action,//
 MOUSE_MOVED_ACTION extends Mousemanager.Action,///
 MOUSE_PRESSED_ACTION extends Mousemanager.Action, //
 MOUSE_RELEASED_ACTION extends Mousemanager.Action> extends Mousemanager<
  MOUSE_CLICKED_ACTION,//
 MOUSE_DRAGGED_ACTION,//
 Mousemanager.SimpleAction,//
 Mousemanager.SimpleAction, ///
 MOUSE_MOVED_ACTION,///
 MOUSE_PRESSED_ACTION, //
 MOUSE_RELEASED_ACTION,//
 Mousemanager.SimpleAction, //
 Mousemanager.DeltaAction> {

  public void resgisterMouseEntered(Mousemanager.SimpleAction action) {
    super.resgister(super.mouse_action_entered, 0, action);
  }

  public void resgisterMouseExited(Mousemanager.SimpleAction action) {
    super.resgister(super.mouse_action_exited, 0, action);
  }

  public void resgisterMouseWheel(int button, SimpleAction action) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void registerMouseActive(int button, DeltaAction deltaAction) {
    Mousemanager.resgister(mouse_action_active, button, deltaAction);
  }

  public void mouseEntered(MouseEvent e) {
    SimpleAction get = mouse_action_entered.getMap().get(e.getButton());
    if (get != null) {
      get.action();
    }
  }

  public void mouseExited(MouseEvent e) {
    SimpleAction get = mouse_action_exited.getMap().get(e.getButton());
    if (get != null) {
      get.action();
    }
  }

  public void mouseWheelMoved(MouseWheelEvent e) {

  }

  public void mouseActive() {

    long now = System.currentTimeMillis();
    long delta = now - last;
    for (Integer button : active_buttons) {
      DeltaAction get = super.mouse_action_active.getMap().get(button);
      if (get != null) {
        get.action(delta);
      }
    }
    last = now;
  }
 
}
