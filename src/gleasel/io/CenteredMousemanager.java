/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.io;

import java.awt.AWTException;
import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrew
 */
public class CenteredMousemanager extends BasicMousemanager<
  Mousemanager.SimpleAction, //
 Mousemanager.ChangeAction, //
 Mousemanager.ChangeAction, //
 Mousemanager.SimpleAction, //
 Mousemanager.SimpleAction> {

  protected final Component component;
  protected final Robot robot;

  public CenteredMousemanager(Component component) throws AWTException {
    this.component = component;
    this.robot = new Robot();
  }

  public void registerMouseDragged(int button, ChangeAction changeAction) {
    resgister(mouse_action_dragged, button, changeAction);
  }

  public void registerMouseMoved(ChangeAction changeAction) {
    resgister(mouse_action_moved, 0, changeAction);
  }

  public void registerMousePressed(int button, SimpleAction simpleAction) {
    resgister(mouse_action_pressed, button, simpleAction);
  }

  public void registerMouseRelease(int button, SimpleAction simpleAction) {
    resgister(mouse_action_released, button, simpleAction);
  }

  public void registerMouseClicked(int button, SimpleAction simpleAction) {
    resgister(mouse_action_clicked, button, simpleAction);
  }

  @Override
  public void resgisterMouseEntered(Mousemanager.SimpleAction action) {
    throw new InternalError("Never used for this class");
  }

  @Override
  public void resgisterMouseExited(Mousemanager.SimpleAction action) {
    throw new InternalError("Never used for this class");
  }

  @Override
  public void mouseDragged(MouseEvent e) {
    Point diff = getFrameCenter();
    diff.x -= MouseInfo.getPointerInfo().getLocation().x; // Much more accurate compared to e.getLocationOnScreen().x
    diff.y -= MouseInfo.getPointerInfo().getLocation().y;

    for (Integer integer : active_buttons) {
      ChangeAction get = super.mouse_action_dragged.getMap().get(integer);
      if (get != null) {
        get.action(diff);
      }
    }
    centerMouse();
    e.consume();
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    Point diff = getFrameCenter();
    diff.x -= MouseInfo.getPointerInfo().getLocation().x;
    diff.y -= MouseInfo.getPointerInfo().getLocation().y;

    ChangeAction get = super.mouse_action_moved.getMap().get(0);
    if (get != null) {
      get.action(diff);
    }
    centerMouse();
    e.consume();
  }

  @Override
  public void mousePressed(MouseEvent e) {
    SimpleAction get = mouse_action_pressed.getMap().get(e.getButton());
    if (get != null) {
      get.action();
    }
    e.consume();

    this.active_buttons.add(e.getButton());
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    SimpleAction get = mouse_action_released.getMap().get(e.getButton());
    if (get != null) {
      get.action();
    }
    e.consume();

    this.active_buttons.remove(e.getButton());
  }

  @Override
  public void mouseClicked(MouseEvent e) {
    SimpleAction get = mouse_action_clicked.getMap().get(e.getButton());
    if (get != null) {
      get.action();
    }
    e.consume();
  }

  public void centerMouse() {
    if (component.isVisible()) {
      robot.mouseMove(component.getLocationOnScreen().x + component.getWidth() / 2, component.getLocationOnScreen().y + component.getHeight() / 2);
    }
  }

  public Point getFrameCenter() {
    if (component.isVisible()) {
      return new Point(component.getLocationOnScreen().x + component.getWidth() / 2, component.getLocationOnScreen().y + component.getHeight() / 2);
    } else {
      return new Point();
    }
  }

  @Override
  public void registerMouseDragged(int button, SimpleAction changeAction) {
    throw new InternalError("Never used for this class");
  }

  @Override
  public void registerMouseWheel(int button, SimpleAction simpleAction) {
    throw new InternalError("Never used for this class");
  }

  @Override
  public void reset() {
   centerMouse();
  }


}
