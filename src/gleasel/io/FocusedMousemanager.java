/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.io;

import java.awt.Frame;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.MouseEvent;

/**
 *
 * @author andrew
 */
public class FocusedMousemanager extends BasicMousemanager<
  Mousemanager.PointAction,//
  Mousemanager.ChangeAction,//
  Mousemanager.ChangeAction,//
  Mousemanager.PointAction,//
  Mousemanager.PointAction  > {

  private final Point last_point = new Point();


  public void mouseDragged(MouseEvent e) {
    Point point = new Point(e.getPoint());
    Point diff = new Point(point);
    diff.x -= last_point.x;
    diff.y -= last_point.y;
    for (Integer integer : active_buttons) {
      ChangeAction get = mouse_action_dragged.getMap().get(integer);
      if (get != null) {
        get.action(diff);
      }
    }
    e.consume();
    last_point.setLocation(point);
  }

  public void mouseMoved(MouseEvent e) {
    Point point = new Point(e.getPoint());
    Point diff = new Point(point);
    diff.x -= last_point.x;
    diff.y -= last_point.y;
    ChangeAction get = mouse_action_moved.getMap().get(0);
    if (get != null) {
      get.action(diff);
    }
    e.consume();
    last_point.setLocation(point);
  }

  public void mouseClicked(MouseEvent e) {
    Point point = new Point(e.getPoint());
    PointAction get = mouse_action_clicked.getMap().get(e.getButton());
    if (get != null) {
      get.action(point);
    }
    e.consume();
    last_point.setLocation(point);

    this.active_buttons.add(e.getButton());
  }

  public void mousePressed(MouseEvent e) {

    Point point = new Point(e.getPoint());
    PointAction get = mouse_action_pressed.getMap().get(e.getButton());
    if (get != null) {
      get.action(point);
    }
    e.consume();
    last_point.setLocation(point);

    this.active_buttons.add(e.getButton());
  }

  public void mouseReleased(MouseEvent e) {
    Point point = new Point(e.getPoint());
    PointAction get = mouse_action_released.getMap().get(e.getButton());
    if (get != null) {
      get.action(point);
    }
    e.consume();
    last_point.setLocation(point);
    this.active_buttons.remove(e.getButton());
  }

  @Override
  public void registerMouseClicked(int button, PointAction simpleAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void registerMouseDragged(int button, PointAction changeAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void registerMouseMoved(ChangeAction changeAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void registerMousePressed(int button, PointAction simpleAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void registerMouseRelease(int button, PointAction simpleAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void registerMouseWheel(int button, SimpleAction simpleAction) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void reset() {
    
  }

}
