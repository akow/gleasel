package gleasel.io;

import gleasel.io.XRepeatingReleasedEventsFixer.RepeatingReleasedEventsFixer;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.KeyStroke;

/**
 * @Purpose @author kow
 */
public class Keymanager {

  public static abstract class SimpleAction extends Action {

    public abstract void action();
  }

  public static abstract class DeltaAction extends Action {

    public abstract void action(double delta);

  }

  public static abstract class Action {

    public abstract String getDescription();

    public String toString() {
      return getDescription();
    }
  }

  public static class KeyActionSet<T> {

    private HashMap<KeyStroke, T> map;
    private String name;
    private String description;

    public KeyActionSet(String name, String description) {
      this.map = new HashMap<KeyStroke, T>();
      this.name = name;
      this.description = description;

    }

    /**
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
      return description;
    }

    /**
     * @return the map
     */
    public HashMap<KeyStroke, T> getMap() {
      return map;
    }

  }

  private static final boolean USING_LINUX_FIX = true;
  private static final boolean USE_FIX;

  static {
    if (USING_LINUX_FIX && System.getProperty("os.name").equalsIgnoreCase("linux")) {
      USE_FIX = true;
      new RepeatingReleasedEventsFixer().install();
    } else {
      USE_FIX = false;
    }
  }

  private final int[] all_modifiers = new int[]{KeyEvent.ALT_DOWN_MASK, KeyEvent.CTRL_DOWN_MASK, KeyEvent.META_DOWN_MASK, KeyEvent.SHIFT_DOWN_MASK};

  private final KeyActionSet<SimpleAction> key_pressed_action;
  private final KeyActionSet<SimpleAction> key_release_action;
  private final KeyActionSet<DeltaAction> key_active_action;
  //  private final KeyActionSet<SimpleAction> key_typed_action;

  //   private final HashSet<KeyStroke> keyIsNotReleased;
  private final HashSet<KeyStroke> active_keys;
  private final ArrayList<KeyActionSet> all_maps;
  private long last = System.currentTimeMillis();

  public Keymanager() {
    this.all_maps = new ArrayList<KeyActionSet>();
    this.all_maps.add(this.key_pressed_action = new KeyActionSet<SimpleAction>("Key Pressed", "Mappings for when a key is pressed"));
    this.all_maps.add(this.key_release_action = new KeyActionSet<SimpleAction>("Key Released", "Mappings for when a key is released"));
    this.all_maps.add(this.key_active_action = new KeyActionSet<DeltaAction>("Key Active", "Mappings for when a key is being held down"));
    //       this.all_maps.add(this.key_typed_action = new KeyActionSet<SimpleAction>("Key Typed", "Mappings for when a key is repeatedly typed"));
    this.active_keys = new HashSet<KeyStroke>();
    //    this.keyIsNotReleased = new HashSet<KeyStroke>();
  }

  private static <T extends Action> void registerKey(KeyStroke keyStroke, T action, KeyActionSet keyActionSet) {
    if (USE_FIX) {
      keyStroke = makePressedReleasedCommon(keyStroke);
    }
    if (keyActionSet.getMap().containsKey(keyStroke)) {
      Logger.getLogger(Keymanager.class.getName()).log(Level.SEVERE, "Key is already registered", new Exception("Key(" + keyStroke.getKeyChar() + "," + keyStroke.getModifiers() + "): " + keyActionSet.getMap().get(keyStroke)));
    } else {
      keyActionSet.getMap().put(keyStroke, action);
    }
  }

  public void registerKeyPressed(KeyStroke keyStroke, SimpleAction action) {
    registerKey(keyStroke, action, key_pressed_action);
  }

  public void registerKeyReleased(KeyStroke keyStroke, SimpleAction action) {
    registerKey(keyStroke, action, key_release_action);
  }

  public void registerKeyActive(KeyStroke keyStroke, DeltaAction action) {// throws InvalidParameterException {
//    if (keyStroke.getModifiers() != 0) {
//      throw new InvalidParameterException("Active keys cannot be paired with a modifier: " + keyStroke.toString());
//    }
    registerKey(keyStroke, action, key_active_action);
  }

//    public void registerKeyTyped(KeyStroke keyStroke, SimpleAction action) {
//        registerKey(keyStroke, action, key_typed_action);
//    }
  public void clear() {
    for (KeyActionSet mapAndName : this.all_maps) {
      mapAndName.getMap().clear();
    }
  }

//    /**
//     * Clears all keys associated with Class clazz
//     *
//     * @param clazz
//     */
//    public void clear(Class<?> clazz) {
//        for (KeyActionSet keyActionSet : this.all_maps) {
//            HashMap map = keyActionSet.getMap();
//            for (Object object : map.entrySet()) {
//                if (((Map.Entry<KeyStroke, Action>) object).getValue().getClazz().equals(clazz)) {
//                    keyActionSet.getMap().remove(((Map.Entry<Integer, MyKeyEvent>) object).getKey());
//                }
//            }
//        }
//    }
  public void keyPressed(KeyEvent ke) {
    KeyStroke keyStrokeForEvent = KeyStroke.getKeyStrokeForEvent(ke);
    if (USE_FIX) {
      keyStrokeForEvent = makePressedReleasedCommon(keyStrokeForEvent);
      if (this.active_keys.contains(keyStrokeForEvent)) {
        // Avoids repeated presses
        return;
      } else {
        for (int i : all_modifiers) {
          for (KeyStroke keyStroke : active_keys) {
            if (keyStroke.getKeyCode() == ke.getKeyCode()) {
              active_keys.remove(keyStroke);
            }
          }
        }
        this.active_keys.add(keyStrokeForEvent);
      }
    } else {
      this.active_keys.add(keyStrokeForEvent);
    }
    SimpleAction get = key_pressed_action.getMap().get(keyStrokeForEvent);
    if (get != null) {
      get.action();
    }
  }

  public void keyReleased(KeyEvent ke) {
    KeyStroke keyStrokeForEvent = KeyStroke.getKeyStrokeForEvent(ke);
    if (USE_FIX) {
      keyStrokeForEvent = makePressedReleasedCommon(keyStrokeForEvent);
    }

    SimpleAction get = key_release_action.getMap().get(keyStrokeForEvent);
    if (get != null) {
      get.action();
    }
//    for (int i : all_modifiers) {
//      this.active_keys.remove(KeyStroke.getKeyStroke(keyStrokeForEvent.getKeyCode(), i));
//    }
    this.active_keys.remove(keyStrokeForEvent);
  }

  public void keyActive() {
    long now = System.currentTimeMillis();
    long delta = now - last;
    for (KeyStroke keyStroke : this.active_keys) {
      DeltaAction get = key_active_action.getMap().get(keyStroke);
      if (get != null) {
        get.action(delta * 0.001);
      }
    }
    last = now;
  }

  public void keyTyped(KeyEvent ke) {
//        KeyStroke keyStrokeForEvent = KeyStroke.getKeyStrokeForEvent(ke);
//        if (USE_FIX) {
//            keyStrokeForEvent = makePressedReleasedCommon(keyStrokeForEvent);
//        }
//        SimpleAction get = key_typed_action.getMap().get(keyStrokeForEvent);
//        if (get != null) {
//            get.action();
//        }
  }

  private static KeyStroke makePressedReleasedCommon(KeyStroke ks) {
    return KeyStroke.getKeyStroke(ks.getKeyCode(), ks.getModifiers(), false);
  }

  public String toString() {
    String s = "";
    for (KeyActionSet keyActionSet : this.all_maps) {
      s += "Key state: " + keyActionSet.getName() + ". Description: " + keyActionSet.getDescription() + "\n";
      for (Object entry : keyActionSet.getMap().entrySet()) {
        s += "\t" + (((Map.Entry<KeyStroke, Action>) entry).getKey()) + " => " + ((Map.Entry<KeyStroke, Action>) entry).getValue() + "\n";
      }
    }
    return s;
  }

  public void reset() {

  }
}
