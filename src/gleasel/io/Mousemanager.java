/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gleasel.io;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author andrew
 */
public abstract class Mousemanager<
  MOUSE_CLICKED_ACTION extends Mousemanager.Action,//
    MOUSE_DRAGGED_ACTION extends Mousemanager.Action,//
    MOUSE_ENTERED_ACTION extends Mousemanager.Action,//
    MOUSE_EXITED_ACTION extends Mousemanager.Action, ///
    MOUSE_MOVED_ACTION extends Mousemanager.Action,///
    MOUSE_PRESSED_ACTION extends Mousemanager.Action, //
    MOUSE_RELEASED_ACTION extends Mousemanager.Action,//
    MOUSE_WHEEL_ACTION extends Mousemanager.Action, //
    BUTTON_ACTIVE_ACTION extends Mousemanager.Action> {

  public static abstract class SimpleAction extends Action {

    public abstract void action();
  }

  public static abstract class DeltaAction extends Action {

    public abstract void action(long delta);
  }

  public static abstract class ChangeAction extends Action {

    public abstract void action(Point diff);
  }

  public static abstract class PointAction extends Action {

    public abstract void action(Point point);
  }

  public static abstract class Action {
  }

  public static class MouseActionSet<T, A extends Action> {

    private HashMap<T, A> map;
    private String name;
    private String description;

    public MouseActionSet(String name, String description) {
      this.map = new HashMap<T, A>();
      this.name = name;
      this.description = description;

    }

    /**
     * @return the name
     */
    public String getName() {
      return name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
      return description;
    }

    /**
     * @return the map
     */
    public HashMap<T, A> getMap() {
      return map;
    }

  }

  protected final HashSet<MouseActionSet> all_maps;
  protected final MouseActionSet<Integer, MOUSE_CLICKED_ACTION> mouse_action_clicked;
  protected final MouseActionSet<Integer, MOUSE_DRAGGED_ACTION> mouse_action_dragged;
  protected final MouseActionSet<Integer, MOUSE_ENTERED_ACTION> mouse_action_entered;
  protected final MouseActionSet<Integer, MOUSE_EXITED_ACTION> mouse_action_exited;
  protected final MouseActionSet<Integer, MOUSE_MOVED_ACTION> mouse_action_moved;
  protected final MouseActionSet<Integer, MOUSE_PRESSED_ACTION> mouse_action_pressed;
  protected final MouseActionSet<Integer, MOUSE_RELEASED_ACTION> mouse_action_released;
  protected final MouseActionSet<Integer, MOUSE_WHEEL_ACTION> mouse_action_wheel;
  protected final MouseActionSet<Integer, BUTTON_ACTIVE_ACTION> mouse_action_active;
  protected final HashSet<Integer> active_buttons;
  protected long last = System.currentTimeMillis();

  public Mousemanager() {

    this.all_maps = new HashSet<MouseActionSet>();
    this.all_maps.add(mouse_action_clicked = new MouseActionSet<Integer, MOUSE_CLICKED_ACTION>("Mouse Clicked", null));
    this.all_maps.add(mouse_action_dragged = new MouseActionSet<Integer, MOUSE_DRAGGED_ACTION>("Mouse Dragged", null));
    this.all_maps.add(mouse_action_entered = new MouseActionSet<Integer, MOUSE_ENTERED_ACTION>("Mouse Entered", null));
    this.all_maps.add(mouse_action_exited = new MouseActionSet<Integer, MOUSE_EXITED_ACTION>("Mouse Exited", null));
    this.all_maps.add(mouse_action_moved = new MouseActionSet<Integer, MOUSE_MOVED_ACTION>("Mouse Moved", null));
    this.all_maps.add(mouse_action_pressed = new MouseActionSet<Integer, MOUSE_PRESSED_ACTION>("Mouse Pressed", null));
    this.all_maps.add(mouse_action_released = new MouseActionSet<Integer, MOUSE_RELEASED_ACTION>("Mouse Released", null));
    this.all_maps.add(mouse_action_wheel = new MouseActionSet<Integer, MOUSE_WHEEL_ACTION>("Mouse Wheel", null));
    this.all_maps.add(mouse_action_active = new MouseActionSet<Integer, BUTTON_ACTIVE_ACTION>("Mouse Active", null));
    this.active_buttons = new HashSet<Integer>();
  }

  protected static void resgister(MouseActionSet mouseActionSet, int button, Action action) {
    if (mouseActionSet.getMap().containsKey(button)) {
      throw new InternalError("Button is already registered. Button(" + button + "): " + mouseActionSet.getMap().get(button));
    } else {
      mouseActionSet.getMap().put(button, action);
    }
  }

  public abstract void mouseDragged(MouseEvent e);

  public abstract void mouseMoved(MouseEvent e);

  public abstract void mouseClicked(MouseEvent e);

  public abstract void mousePressed(MouseEvent e);

  public abstract void mouseReleased(MouseEvent e);

  public abstract void mouseEntered(MouseEvent e);

  public abstract void mouseExited(MouseEvent e);

  public abstract void mouseWheelMoved(MouseWheelEvent e);

  public abstract void mouseActive();

  public abstract void registerMouseClicked(int button, MOUSE_CLICKED_ACTION simpleAction);

  public abstract void registerMouseDragged(int button, MOUSE_CLICKED_ACTION changeAction);

  public abstract void resgisterMouseEntered(MOUSE_ENTERED_ACTION action);

  public abstract void resgisterMouseExited(MOUSE_EXITED_ACTION action);

  public abstract void registerMouseMoved(MOUSE_MOVED_ACTION changeAction);

  public abstract void registerMousePressed(int button, MOUSE_PRESSED_ACTION simpleAction);

  public abstract void registerMouseRelease(int button, MOUSE_RELEASED_ACTION simpleAction);

  public abstract void registerMouseWheel(int button, MOUSE_WHEEL_ACTION simpleAction);

  public abstract void registerMouseActive(int button, BUTTON_ACTIVE_ACTION simpleAction);

  public abstract void reset();
  
}
